import os

version=os.environ["version"]
infile_src = '/var/lib/mysql-files/gencode.v'+version+'.annotation.gtf.data'
outfile_src='/var/lib/mysql-files/gencode.v' + version + '.annotation.gtf.data.transcripts'
infile = open(infile_src, 'r')
outfile = open(outfile_src, 'w')

for c, line in enumerate(infile):
    fields = line.split('\t')
    attributes = fields[8].split(";")
    add_features = {}
    add_features_str = ""
    write_transcript = False
    for attribute in attributes:
        # print(attribute)
        attribute = attribute.strip()
        if attribute.startswith(" "):
            attribute = attribute[1:]
        values = attribute.split(" ")
        if len(values) > 1:
            # print("values ",values)
            key = values[0]
            val = values[1]
            if val.startswith("\""):
                val = val[1:-1]
            # print("assign ",key,":",val)

            if key not in add_features:
                add_features[key] = []
            add_features[key].append(val)

    add_features_str += str(c) + "\t"
    if "gene_id" in add_features:
        add_features_str += add_features["gene_id"][0] + "\t"
    else:
        add_features_str += "\t"

    if "gene_name" in add_features:
        add_features_str += add_features["gene_name"][0] + "\t"
    else:
        add_features_str += "\t"

    if "transcript_id" in add_features:
        add_features_str += add_features["transcript_id"][0] + "\t"
    else:
        add_features_str += "\t"

    if "transcript_type" in add_features:
        add_features_str += add_features["transcript_type"][0] + "\t"
    else:
        add_features_str += "\t"

    if "transcript_name" in add_features:
        add_features_str += add_features["transcript_name"][0] + "\t"
    else:
        add_features_str += "\t"

    if "protein_id" in add_features:
        add_features_str += add_features["protein_id"][0] + "\t"
    else:
        add_features_str += "\t"

    if "transcript_support_level" in add_features:
        add_features_str += add_features["transcript_support_level"][0] + "\t"
    else:
        add_features_str += "\t"

    if "hgnc_id" in add_features:
        add_features_str += add_features["hgnc_id"][0] + "\t"
    else:
        add_features_str += "\t"

    if "havana_gene" in add_features:
        add_features_str += add_features["havana_gene"][0] + "\t"
    else:
        add_features_str += "\t"

    if "havana_transcript" in add_features:
        add_features_str += add_features["havana_transcript"][0] + "\t"
    else:
        add_features_str += "\t"

    newline = add_features_str + line
    outfile.write(newline)
infile.close()
outfile.close()
