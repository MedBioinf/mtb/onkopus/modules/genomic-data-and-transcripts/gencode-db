#!/bin/bash

# Download database files if they cannot be found
SOURCE_BASE="https://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_${VERSION}/"
SOURCE="gencode.v${VERSION}.annotation.gtf.gz"
FILE="gencode.v${VERSION}.annotation.gtf"
FILE_PROCESSED="${FILE}.data"
FILE_PROCESSED_IDS="${FILE_PROCESSED}.ids"
FILEPATH="/var/lib/mysql-files/${FILE}"
FILEPATH_PROCESSED="/var/lib/mysql-files/${FILE_PROCESSED}"
export version=${VERSION}

# GENCODE annotations
if ! test -f "${FILEPATH}"; then
  echo "Annotation database file not found. Downloading file..."
  wget -v "${SOURCE_BASE}${SOURCE}" -P /var/lib/mysql-files
  gunzip "${FILEPATH}.gz"
fi

if  ! test -f "${FILEPATH_PROCESSED}"; then
  tail -n +6 ${FILEPATH} > "${FILEPATH_PROCESSED}"
fi

# RefSeq mapping
if ! test -f "/var/lib/mysql-files/gencode.v${VERSION}.metadata.RefSeq"; then
  echo "Mapping to RefSeq transcripts file not found. Downloading file..."
  wget -v "${SOURCE_BASE}gencode.v${VERSION}.metadata.RefSeq.gz" -P /var/lib/mysql-files
  gunzip /var/lib/mysql-files/gencode.v${VERSION}.metadata.RefSeq.gz
fi

# Protein sequence table
if ! test -f "/var/lib/mysql-files/gencode.v${VERSION}.pc_translations.fa"; then
  echo "Protein sequence file not found. Downloading file..."
  wget -v "${SOURCE_BASE}gencode.v${VERSION}.pc_translations.fa.gz" -P /var/lib/mysql-files
  gunzip /var/lib/mysql-files/gencode.v${VERSION}.pc_translations.fa.gz
fi

# Transcript sequences
if ! test -f "/var/lib/mysql-files/gencode.v${VERSION}.transcripts.fa"; then
  echo "Transcript sequence file not found. Downloading file..."
  wget -v "${SOURCE_BASE}gencode.v${VERSION}.transcripts.fa.gz" -P /var/lib/mysql-files
  gunzip /var/lib/mysql-files/gencode.v${VERSION}.transcripts.fa.gz
fi

# PDB file
if ! test -f "/var/lib/mysql-files/gencode.v${VERSION}.metadata.PDB"; then
  echo "PDB ID file not found. Downloading file..."
  wget -v "${SOURCE_BASE}gencode.v${VERSION}.metadata.PDB.gz" -P /var/lib/mysql-files
  gunzip /var/lib/mysql-files/gencode.v${VERSION}.metadata.PDB.gz
fi

# SwissProt Entries
if ! test -f "/var/lib/mysql-files/gencode.v${VERSION}.metadata.SwissProt"; then
  echo "SwissProt file not found. Downloading file..."
  wget -v "${SOURCE_BASE}gencode.v${VERSION}.metadata.SwissProt.gz" -P /var/lib/mysql-files
  gunzip /var/lib/mysql-files/gencode.v${VERSION}.metadata.SwissProt.gz
fi

# TrEMBL entries
if ! test -f "/var/lib/mysql-files/gencode.v${VERSION}.metadata.TrEMBL"; then
  echo "TrEMBL file not found. Downloading file..."
  wget -v "${SOURCE_BASE}gencode.v${VERSION}.metadata.TrEMBL.gz" -P /var/lib/mysql-files
  gunzip /var/lib/mysql-files/gencode.v${VERSION}.metadata.TrEMBL.gz
fi

PW=${MARIADB_ROOT_PASSWORD}
mariadb -u "${USER_NAME}" -p"${PW}" -e "DROP DATABASE ${DB_NAME};"

/opt/venv/bin/python3 ${SCRIPT_DIR}/generate_gencode_data.py
/opt/venv/bin/python3 ${SCRIPT_DIR}/generate_transcript_table.py
/opt/venv/bin/python3 ${SCRIPT_DIR}/generate_mane_select_transcript_table.py
export gencode_psequence_file="/var/lib/mysql-files/gencode.v${VERSION}.pc_translations.fa"
export gencode_psequence_outfile="/var/lib/mysql-files/gencode.v${VERSION}.pc_translations.csv"
/opt/venv/bin/python3 ${SCRIPT_DIR}/generate_protein_sequence_file.py
export gencode_transcripts_file="/var/lib/mysql-files/gencode.v${VERSION}.transcripts.fa"
export gencode_transcripts_outfile="/var/lib/mysql-files/gencode.v${VERSION}.transcripts.csv"
/opt/venv/bin/python3 ${SCRIPT_DIR}/generate_transcript_sequences.py

# Generate database
echo "Generating annotation database tables..."
mariadb -u "${USER_NAME}" -p"${PW}" -e "CREATE DATABASE ${DB_NAME};"
mariadb -u "${USER_NAME}" -p"${PW}" -e "USE ${DB_NAME};"
mariadb -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "create table gencode_data (id varchar(40) primary key,gene_id varchar(100),gene_name varchar(150),seqname varchar(20),source varchar(15),feature varchar(20),start_pos int,end_pos int,score varchar(10),strand varchar(5),frame varchar(5),attribute varchar(1000));"
mariadb -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "load data infile \"${FILEPATH}.data.ids\" into table gencode_data fields terminated by '\t' enclosed by '' lines terminated by '\n' ignore 1 rows;"
mariadb -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "create index idx_gene_name on gencode_data (gene_name);"
mariadb -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "create index idx_start_pos on gencode_data (start_pos);"
mariadb -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "create index idx_end_pos on gencode_data (end_pos);"
mariadb -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "create index index_seqname_start_end on gencode_data (seqname,start_pos,end_pos);"

# Generate gene position table
${SCRIPT_DIR}/generate_gene_position_data.sh
python3 ${SCRIPT_DIR}/generate_gene_position_table.py
mariadb -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "create table genomic_positions (id varchar(40) primary key,gene_id varchar(100),gene_name varchar(150),seqname varchar(20),source varchar(15),feature varchar(20),start_pos int,end_pos int,score varchar(10),strand varchar(5),frame varchar(5),attribute varchar(1000) );"
mariadb -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "load data infile '/var/lib/mysql-files/gene_positions_gene_names.gtf' into table genomic_positions fields terminated by '\t' enclosed by '' lines terminated by '\n' ignore 1 rows;"
mariadb -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "create index idgp_gene_name on genomic_positions (gene_name);"
mariadb -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "create index idgp_start_pos on genomic_positions (start_pos);"
mariadb -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "create index idgp_end_pos on genomic_positions (end_pos);"

# Generate transcript table
mariadb -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "create table transcripts (id varchar(40) primary key,gene_id varchar(100),gene_name varchar(150),transcript_id varchar(40),transcript_type varchar(80),transcript_name varchar(30),protein_id varchar(40),transcript_support_level varchar(10),hgnc_id varchar(15),havana_gene varchar(30),havana_transcript varchar(30),seqname varchar(20),source varchar(15),feature varchar(20),start_pos int,end_pos int,score varchar(10),strand varchar(5),frame varchar(5),attribute varchar(1000) );"
mariadb -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "load data infile '/var/lib/mysql-files/gencode.v${VERSION}.annotation.gtf.data.transcripts' into table transcripts fields terminated by '\t' enclosed by '' lines terminated by '\n' ignore 1 rows;"
mariadb -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "create index transcripts_gene_name on transcripts (gene_name);"
mariadb -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "create index transcripts_start_pos on transcripts (start_pos);"
mariadb -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "create index transcripts_end_pos on transcripts (end_pos);"

# Generate mane select transcript table
mariadb -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "create table mane_select_transcripts (id varchar(40) primary key,gene_id varchar(100),gene_name varchar(150),transcript_id varchar(40),transcript_type varchar(80),transcript_name varchar(30),protein_id varchar(40),transcript_support_level varchar(10),hgnc_id varchar(15),havana_gene varchar(30),havana_transcript varchar(30),seqname varchar(20),source varchar(15),feature varchar(20),start_pos int,end_pos int,score varchar(10),strand varchar(5),frame varchar(5),attribute varchar(1000) );"
mariadb -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "load data infile '/var/lib/mysql-files/gencode.v${VERSION}.annotation.gtf.data.mane_select_transcripts' into table mane_select_transcripts fields terminated by '\t' enclosed by '' lines terminated by '\n' ignore 1 rows;"
mariadb -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "create index mane_select_transcripts_gene_name on mane_select_transcripts (gene_name);"
mariadb -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "create index mane_select_transcripts_start_pos on mane_select_transcripts (start_pos);"
mariadb -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "create index mane_select_transcripts_end_pos on mane_select_transcripts (end_pos);"

# Generate RefSeq mapping table
mariadb -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "create table refseq_mapping (id int auto_increment,ensembl_transcript varchar(50),refseq_transcript varchar(50),refseq_protein varchar(50),primary key(id));"
mariadb -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "LOAD DATA INFILE '/var/lib/mysql-files/gencode.v${VERSION}.metadata.RefSeq' into table refseq_mapping fields terminated by '\t' enclosed by '' lines terminated by '\n' ignore 0 rows (ensembl_transcript,refseq_transcript,refseq_protein);"
mariadb -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "create index refseq_mapping_index on refseq_mapping (ensembl_transcript);"

# Generate transcript sequences
mariadb -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "CREATE TABLE transcript_sequences (transcript_id varchar(50) primary key, sequence longtext);"
mariadb -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "LOAD DATA INFILE '/var/lib/mysql-files/gencode.v${VERSION}.transcripts.csv' into table transcript_sequences fields terminated by ',' enclosed by '' lines terminated by '\n' ignore 0 rows;"
mariadb -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "create index transcript_sequence_index on transcript_sequences (transcript_id);"

# Generate protein sequence table
mariadb -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "CREATE TABLE protein_sequences (transcript_id varchar(50) primary key, sequence text);"
mariadb -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "LOAD DATA INFILE '/var/lib/mysql-files/gencode.v${VERSION}.pc_translations.csv' into table protein_sequences fields terminated by ',' enclosed by '' lines terminated by '\n' ignore 0 rows;"
mariadb -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "create index protein_sequence_index on protein_sequences (transcript_id);"

# Generate PDB ID table
mariadb -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "CREATE TABLE pdb_ids (transcript_id varchar(50), pdb_id varchar(30), primary key(transcript_id,pdb_id));"
mariadb -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "LOAD DATA INFILE '/var/lib/mysql-files/gencode.v${VERSION}.metadata.PDB' into table pdb_ids fields terminated by '\t' enclosed by '' lines terminated by '\n' ignore 0 rows;"
mariadb -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "create index pdb_id_index on pdb_ids (transcript_id);"

# Generate SwissProt table
mariadb -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "CREATE TABLE swissprot_ids (transcript_id varchar(50), uniprot_id varchar(30), swiss_prot_id varchar(30), primary key(transcript_id,swiss_prot_id));"
mariadb -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "LOAD DATA INFILE '/var/lib/mysql-files/gencode.v${VERSION}.metadata.SwissProt' into table swissprot_ids fields terminated by '\t' enclosed by '' lines terminated by '\n' ignore 0 rows;"
mariadb -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "create index swiss_prot_index on swissprot_ids (transcript_id);"

# Generate TrEMBL table
mariadb -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "CREATE TABLE trembl_ids (transcript_id varchar(50), uniprot_id varchar(30), trembl_id varchar(30), primary key(transcript_id,trembl_id));"
mariadb -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "LOAD DATA INFILE '/var/lib/mysql-files/gencode.v${VERSION}.metadata.TrEMBL' into table trembl_ids fields terminated by '\t' enclosed by '' lines terminated by '\n' ignore 0 rows;"
mariadb -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "create index trembl_index on trembl_ids (transcript_id);"
