import os

version=os.environ["version"]
infile_src = '/var/lib/mysql-files/gencode.v' + version + '.annotation.gtf.data'
outfile_src='/var/lib/mysql-files/gencode.v' + version + '.annotation.gtf.data.ids'
infile = open(infile_src, 'r')
outfile = open(outfile_src, 'w')

for c, line in enumerate(infile):
    fields = line.split('\t')
    attributes = fields[8].split(";")
    add_features = {}
    add_features_str = ""
    for attribute in attributes:
        # print(attribute)
        attribute = attribute.strip()
        if attribute.startswith(" "):
            attribute = attribute[1:]
        values = attribute.split(" ")
        if len(values) > 1:
            # print("values ",values)
            key = values[0]
            val = values[1]
            if val.startswith("\""):
                val = val[1:-1]
            # print("assign ",key,":",val)

            add_features[key] = val

    add_features_str += str(c) + "\t"
    if "gene_id" in add_features:
        add_features_str += add_features["gene_id"] + "\t"
    else:
        add_features_str += "\t"

    if "gene_name" in add_features:
        add_features_str += add_features["gene_name"] + "\t"
    else:
        add_features_str += "\t"

    newline = add_features_str + line
    outfile.write(newline)
infile.close()
outfile.close()
