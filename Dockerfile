FROM mariadb:11.1.2-jammy

RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install -y wget python3 nano less python3-dev python3-pip python3-venv git libmagic1

RUN python3 -m pip install --upgrade pip
RUN python3 -m venv /opt/venv
RUN /opt/venv/bin/python3 -m pip install --upgrade pip
RUN /opt/venv/bin/python3 -m pip install pyliftover requests plotly matplotlib scikit-learn pandas adagenes

WORKDIR /modules
#RUN git clone https://gitlab.gwdg.de/MedBioinf/mtb/vcfcli.git
#RUN /opt/venv/bin/python3 -m pip install -e /modules/vcfcli

COPY ./preprocess.sh /mnt/preprocess.sh
COPY ./generate_gencode_data.py /mnt/generate_gencode_data.py
COPY ./generate_gene_position_data.sh /mnt/generate_gene_position_data.sh
COPY ./generate_gene_position_table.py /mnt/generate_gene_position_table.py
COPY ./generate_transcript_table.py /mnt/generate_transcript_table.py
COPY ./generate_mane_select_transcript_table.py /mnt/generate_mane_select_transcript_table.py
COPY ./generate_protein_sequence_file.py /mnt/generate_protein_sequence_file.py
COPY ./generate_transcript_sequences.py /mnt/generate_transcript_sequences.py

EXPOSE 3306

