import os
import adagenes as ag


def generate_transcript_sequence_csv_file(infile,outfile):
    """

    :param infile:
    :return:
    """
    bframe = ag.FASTAReader().read_file(infile)
    # print(bframe.data.keys())
    data = {}
    for key in bframe.data.keys():
        transcript_id = key.split("|")[0]
        if transcript_id not in data.keys():
            data[transcript_id] = bframe.data[key]
        else:
            print("key already found: ", transcript_id)
    bframe.data = data

    mapping = {
        "sequence":[]
    }
    ag.TSVWriter().write_to_file(outfile,bframe,mapping=mapping)


infile=os.environ["gencode_transcripts_file"]
outfile=os.environ["gencode_transcripts_outfile"]
generate_transcript_sequence_csv_file(infile,outfile)
