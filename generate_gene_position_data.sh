
FILEPATH="/var/lib/mysql-files"

awk 'BEGIN{FS="\t"; OFS="\t"}{if($3 == "gene") print $1, $2, $3, $4, $5, $6, $7, $8, $9}' "${FILEPATH}/gencode.v${VERSION}.annotation.gtf" > "${FILEPATH}/gene_positions.gtf"

