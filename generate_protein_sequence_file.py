import os
import adagenes as ag


def generate_protein_sequence_csv_file(infile,outfile):
    """

    :param infile:
    :return:
    """
    bframe = ag.clients.FASTAReader().read_file(infile)
    # print(bframe.data.keys())
    data = {}
    for key in bframe.data.keys():
        transcript_id = key.split("|")[1]
        if transcript_id not in data.keys():
            data[transcript_id] = bframe.data[key]
        else:
            print("key already found: ", transcript_id)
    bframe.data = data

    mapping = {
        "sequence":[]
    }
    ag.TSVWriter().write_to_file(outfile,bframe,mapping=mapping)


infile=os.environ["gencode_psequence_file"]
outfile=os.environ["gencode_psequence_outfile"]
generate_protein_sequence_csv_file(infile,outfile)
